# Visual studio code web project template with Type-Script language support

## Prerequisites

* [Node.js and NPM](https://nodejs.org)
* Http-server (npm install -g http-server)
* TypeScript compiler (npm install -g typescript)
* (Optionally) TSLint (npm install -g tslint typescript)

## Getting started

1. Download and copy template folder to your workspace
2. Open template folder in VS code
3. By selecting - Tasks->Run Task->http-server - you will start local http-server at port 8080. So for now you can debug your own application in VS code
4. By selecting - Tasks->Run Task->tsc - you will start TypeScript watcher that recompile your files when you change or add new .ts files

## Project structure:

### HTML:

* index.html contains all stater tags

### CSS:

* main.css loads all outer css modules
* style.css contains all user permamently code
* responsive.css contains all user's media queries for different screens

### JavaScript and TypeScript:

* Project using [RequireJS](http://requirejs.org/) to AMD module system support
* For all scripts main root folder is "/scripts"
* main.js contains all precompiled TypeScript code
* Program.ts is a starter class and the Main() function is a start point of a web application ( like in C# :) )

### Other:

* So there are many settings files with linter, typescript, git, etc configurations
